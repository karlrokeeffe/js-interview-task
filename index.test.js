const { network } = require("./network");
const calculateConnections = require("./index");

test("Calculates all connections from London", async () => {
  const connections = await calculateConnections("london");
  expect(connections).toEqual(network);
});

// Uncomment these test to check that the same network is
// generated no matter what city we start at.

// test("Calculates all connections from Paris", async () => {
//   const connections = await calculateConnections("paris");
//   expect(connections).toEqual(network);
// });

// test("Calculates all connections from Brussels", async () => {
//   const connections = await calculateConnections("brussels");
//   expect(connections).toEqual(network);
// });

// test("Calculates all connections from Amsterdam", async () => {
//   const connections = await calculateConnections("amsterdam");
//   expect(connections).toEqual(network);
// });

// test("Calculates all connections from Madrid", async () => {
//   const connections = await calculateConnections("madrid");
//   expect(connections).toEqual(network);
// });

// test("Calculates all connections from Zurich", async () => {
//   const connections = await calculateConnections("zurich");
//   expect(connections).toEqual(network);
// });

// test("Calculates all connections from Berlin", async () => {
//   const connections = await calculateConnections("berlin");
//   expect(connections).toEqual(network);
// });

// test("Calculates all connections from Rome", async () => {
//   const connections = await calculateConnections("rome");
//   expect(connections).toEqual(network);
// });

// test("Calculates all connections from Warsaw", async () => {
//   const connections = await calculateConnections("warsaw");
//   expect(connections).toEqual(network);
// });
