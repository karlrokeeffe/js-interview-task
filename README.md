# Mapping the (fictional) Cross European Rail Network from a single node

In this task you will be writing code to map out the structure of the fictional Cross European Rail Network. This rail network connects capital cities across Europe.

Given the name of a single city the aim is to build up an object that maps every city to its connections like so:

```
network = {
  london: ["paris", "brussels", "amsterdam"],
  paris: ["london", "brussels", "madrid", "zurich"],
  brussels: ["london", "paris", "amsterdam"],
  amsterdam: ["london", "brussels", "berlin"],
  madrid: ["paris"],
  zurich: ["paris", "rome", "prague"],
  berlin: ["amsterdam", "warsaw"],
  rome: ["zurich"],
  prague: ["zurich", "warsaw"],
  warsaw: ["berlin", "prague"]
}
```

Each city only knows of connections to its immediate neighbours. Each city contains an API server you can call to get its connections. The urls for these API servers follow the pattern `https://${cityName}.cross-europe.rail/connections`

You are provided with a `mockFetch` function that given an API server url will return the connected cities as an array of names.

For example:

```
mockFetch(`https://london.cross-europe.rail/connections`)
```

Returns:

```
["paris", "brussels", "amsterdam"]
```

You can then replace `london` in the API call with the name of another city to get its connections.

## Getting Started

Install dependencies and start tests:

```
yarn && yarn test
```

Then fill out the `calculateConnections` function to map out the rail network. The function should return a promise that resolves to an object that equals the `network` object in the `network.js` file.

## Miscellaneous

Feel free to:

- Use Google as much as you want.
- Copy and paste code you find online.
- Install and use any libraries you would find helpful.
