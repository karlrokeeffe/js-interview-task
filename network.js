const network = {
  london: ["paris", "brussels", "amsterdam"],
  paris: ["london", "brussels", "madrid", "zurich"],
  brussels: ["london", "paris", "amsterdam"],
  amsterdam: ["london", "brussels", "berlin"],
  madrid: ["paris"],
  zurich: ["paris", "rome", "prague"],
  berlin: ["amsterdam", "warsaw"],
  rome: ["zurich"],
  prague: ["zurich", "warsaw"],
  warsaw: ["berlin", "prague"]
};

const delay = duration => new Promise(resolve => setTimeout(resolve, duration));

const mockFetch = async (url) => {
  const name = url.match(/^https:\/\/([a-z]+)\.cross-europe\.rail\/connections$/)[1];

  // Simulate the  time the network request/response takes.
  await delay(Math.random() * 100);
  return network[name]
};

module.exports = { network, mockFetch };
